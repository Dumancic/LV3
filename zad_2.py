# -*- coding: utf-8 -*-
"""
Created on Thu Dec 10 17:05:34 2015

@author: Andrija
"""


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

mtcars = pd.read_csv('mtcars.csv')
mtcyl4=0                            
mtcyl6=0
mtcyl8=0
amcars=0
mmcars=0
wtcyl4=0
wtcyl6=0
wtcyl8=0
mpg0=0
mpg1=0
hp1=0
hp0=0
qsec0=0
qsec1=0

mtcyl4+=mtcars[mtcars.cyl==4].mpg   
mtcyl6+=mtcars[mtcars.cyl==6].mpg
mtcyl8+=mtcars[mtcars.cyl==8].mpg
wtcyl4+=mtcars[mtcars.cyl==4].wt
wtcyl6+=mtcars[mtcars.cyl==6].wt
wtcyl8+=mtcars[mtcars.cyl==8].wt
mpg0+=mtcars[mtcars.am == 0].mpg
mpg1+=mtcars[mtcars.am == 1].mpg
hp1+=mtcars[mtcars.am == 1].hp
hp0+=mtcars[mtcars.am == 0].hp
qsec0+=mtcars[mtcars.am == 0].qsec
qsec1+=mtcars[mtcars.am == 1].qsec

svmtcyl4=sum(mtcyl4)/len(mtcyl4)    
svmtcyl6=sum(mtcyl6)/len(mtcyl6)
svmtcyl8=sum(mtcyl8)/len(mtcyl8)

mpg0=mpg0.values        
mpg1=mpg1.values
hp1=hp1.values
hp0=hp0.values
qsec1=qsec1.values
qsec0=qsec0.values


fig = plt.figure()
ax = fig.add_subplot(111)              
width = 2  

rects1 = ax.bar(3, svmtcyl4, width, color='black')      
rects2 = ax.bar(5, svmtcyl6, width, color='red')
rects3 = ax.bar(7, svmtcyl8, width, color='green')

fig = plt.figure()
data=[wtcyl4,wtcyl6,wtcyl8]
plt.boxplot(data, labels=[4, 6, 8])                     


fig = plt.figure()
plt.xlabel("Potrošnja")
for i in range(0,len(mpg0)):
    plt.plot(mpg0[i],0,'bx')                
for i in range(0,len(mpg1)):
    plt.plot(mpg1[i],0,'ro')                

fig = plt.figure()
plt.xlabel("Snaga")
plt.ylabel("Ubrzanje")
for i in range(0,len(hp0)):
    plt.plot(hp0[i],qsec0[i],'bx')      
for i in range(0,len(hp1)):
    plt.plot(hp1[i],qsec1[i],'ro')      

plt.show()
